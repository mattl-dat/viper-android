package com.dat.demo.viper.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dat.demo.viper.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContracts.View {

    private val presenter: MainContracts.Presenter by lazy {
        MainPresenter.newInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_nav.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.item_first -> presenter.onSelectFirstTab()
                R.id.item_second -> presenter.onSelectSecondtab()
            }
            true
        }
    }
}
