package com.dat.demo.viper.entities

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

private const val IS_AUTHENTICATED_KEY = "authenticated"
private const val USERNAME_KEY = "username"

class AppState(
    private val sharedPreferences: SharedPreferences
) {

    constructor(context: Context) : this(
        sharedPreferences = context.getSharedPreferences("app_state", Context.MODE_PRIVATE)
    )

    var username: String?
        get() = sharedPreferences.getString(USERNAME_KEY, null)
        set(value) {
            sharedPreferences.edit {
                putString(USERNAME_KEY, value)
            }
        }

    var isAuthenticated: Boolean
        get() = sharedPreferences.getBoolean(IS_AUTHENTICATED_KEY, false)
        set(value) {
            sharedPreferences.edit {
                putBoolean(IS_AUTHENTICATED_KEY, value)
            }
        }
}