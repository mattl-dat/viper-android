package com.dat.demo.viper.logout

import android.content.Context
import com.dat.demo.viper.entities.AppState

class LogoutInteractor(
    private val appState: AppState
) : LogoutContracts.Interactor {

    constructor(applicationContext: Context) : this(
        appState = AppState(applicationContext)
    )

    override fun logout() {
        appState.isAuthenticated = false
    }
}