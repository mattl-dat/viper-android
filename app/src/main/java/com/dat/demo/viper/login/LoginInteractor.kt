package com.dat.demo.viper.login

import com.dat.demo.viper.entities.AppState
import com.dat.demo.viper.entities.LoginManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginInteractor(
    private val appState: AppState,
    private val loginManager: LoginManager,
    private val output: LoginContracts.Interactor.Output
) : LoginContracts.Interactor {

    override val initialUsername: String?
        get() = appState.username

    override fun login(username: String, password: String) {
        CoroutineScope(Dispatchers.Main).launch {
            when (val result = loginManager.login(username, password)) {
                LoginManager.Result.Success -> output.onLoginSuccess()
                is LoginManager.Result.Failure -> output.onLoginFailure(result.message)
            }
        }
    }

}