package com.dat.demo.viper

import android.app.Activity
import android.content.Intent
import com.dat.demo.viper.login.LoginActivity
import com.dat.demo.viper.main.MainActivity

class LauncherRouter(
    private val activity: Activity
) : LauncherContracts.Router {

    override fun showHome() {
        val intent = Intent(activity, MainActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    override fun showLogin() {
        val intent = Intent(activity, LoginActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}