package com.dat.demo.viper.login

object LoginContracts {

    interface View {
        var progressIndicatorVisible: Boolean

        fun setUsername(username: String?)
        fun showError(message: String)
    }

    interface Presenter {
        fun onStart()
        fun onLogin(username: String, password: String)
    }

    interface Interactor {
        interface Output {
            fun onLoginFailure(message: String)
            fun onLoginSuccess()
        }

        val initialUsername: String?

        fun login(username: String, password: String)
    }

    interface Router {
        fun showHome()
    }
}