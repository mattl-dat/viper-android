package com.dat.demo.viper

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class LauncherActivity : AppCompatActivity(), LauncherContracts.View {

    private val presenter: LauncherContracts.Presenter by lazy {
        LauncherPresenter.newInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

}