package com.dat.demo.viper.logout

import android.app.Activity
import android.content.Context

class LogoutPresenter(
    applicationContext: Context,
    private val view: LogoutContracts.View,
    private val router: LogoutContracts.Router
) : LogoutContracts.Presenter {

    private val interactor: LogoutContracts.Interactor = LogoutInteractor(applicationContext)

    override fun onLogout() {
        interactor.logout()
        router.showLogin()
    }

    companion object {
        fun newInstance(
            activity: Activity,
            view: LogoutContracts.View
        ): LogoutContracts.Presenter = LogoutPresenter(
            applicationContext = activity.applicationContext,
            view = view,
            router = LogoutRouter(activity)
        )
    }
}