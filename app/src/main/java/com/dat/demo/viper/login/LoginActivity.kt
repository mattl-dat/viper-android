package com.dat.demo.viper.login

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.dat.demo.viper.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginContracts.View {

    private val presenter: LoginContracts.Presenter by lazy {
        LoginPresenter.newInstance(this)
    }

    override var progressIndicatorVisible: Boolean
        get() = progress_indicator.isVisible
        set(showIndicator) {
            progress_indicator.isVisible = showIndicator
            login_button.isInvisible = showIndicator
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setOnClickListener {
            val username = username.text.toString()
            val password = password.text.toString()
            presenter.onLogin(username, password)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun setUsername(username: String?) {
        this.username.setText(username)
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}