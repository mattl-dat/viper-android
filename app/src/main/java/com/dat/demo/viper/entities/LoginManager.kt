package com.dat.demo.viper.entities

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class LoginManager(
    private val appState: AppState
) {

    sealed class Result {
        object Success : Result()
        data class Failure(val message: String) : Result()
    }

    suspend fun login(username: String, password: String): Result = withContext(Dispatchers.IO) {
        delay(1_000)
        val message = when {
            username.isEmpty() || password.isEmpty() -> "Missing username or password"
            password == "password" -> "Unable to verify your credentials"
            else -> null
        }
        if (message == null) {
            with(appState) {
                this.isAuthenticated = true
                this.username = username
            }
            Result.Success
        } else {
            Result.Failure(message)
        }
    }

}