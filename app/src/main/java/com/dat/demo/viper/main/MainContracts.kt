package com.dat.demo.viper.main

interface MainContracts {

    interface View

    interface Presenter {
        fun onSelectFirstTab()

        fun onSelectSecondtab()
    }

    interface Router {
        fun showFirstTab()

        fun showSecondTab()
    }
}