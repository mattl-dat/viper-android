package com.dat.demo.viper.logout

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.dat.demo.viper.R
import kotlinx.android.synthetic.main.fragment_logout.*

class LogoutFragment : Fragment(R.layout.fragment_logout), LogoutContracts.View {

    private val presenter: LogoutContracts.Presenter by lazy {
        LogoutPresenter.newInstance(activity = requireActivity(), view = this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logout.setOnClickListener {
            presenter.onLogout()
        }
    }
}