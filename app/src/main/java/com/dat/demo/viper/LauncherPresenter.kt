package com.dat.demo.viper

import android.app.Activity
import android.content.Context
import com.dat.demo.viper.entities.AppState

class LauncherPresenter(
    applicationContext: Context,
    private val view: LauncherContracts.View,
    private val router: LauncherContracts.Router
) : LauncherContracts.Presenter {

    private val interactor = LauncherInteractor(
        appState = AppState(
            applicationContext
        )
    )

    override fun onStart() {
        if (interactor.isAuthenticated) {
            router.showHome()
        } else {
            router.showLogin()
        }
    }

    companion object {
        fun <T> newInstance(activity: T): LauncherPresenter where T : Activity, T : LauncherContracts.View =
            LauncherPresenter(
                applicationContext = activity.applicationContext,
                view = activity,
                router = LauncherRouter(activity)
            )
    }
}