package com.dat.demo.viper

interface LauncherContracts {

    interface View

    interface Presenter {
        fun onStart()
    }

    interface Interactor {
        val isAuthenticated: Boolean
    }

    interface Router {
        fun showHome()
        fun showLogin()
    }
}