package com.dat.demo.viper.logout

interface LogoutContracts {

    interface View

    interface Presenter {
        fun onLogout()
    }

    interface Interactor {
        fun logout()
    }

    interface Router {
        fun showLogin()
    }
}