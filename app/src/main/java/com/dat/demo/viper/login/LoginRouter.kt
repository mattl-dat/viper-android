package com.dat.demo.viper.login

import android.app.Activity
import android.content.Intent
import com.dat.demo.viper.main.MainActivity

class LoginRouter(
    private val activity: Activity
) : LoginContracts.Router {

    override fun showHome() {
        val intent = Intent(activity, MainActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}