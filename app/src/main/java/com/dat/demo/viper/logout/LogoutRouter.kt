package com.dat.demo.viper.logout

import android.app.Activity
import android.content.Intent
import com.dat.demo.viper.login.LoginActivity

class LogoutRouter(
    private val activity: Activity
) : LogoutContracts.Router {

    override fun showLogin() {
        val intent = Intent(activity, LoginActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}