package com.dat.demo.viper

import com.dat.demo.viper.entities.AppState

class LauncherInteractor(
    private val appState: AppState
) : LauncherContracts.Interactor {

    override val isAuthenticated: Boolean
        get() = appState.isAuthenticated
}