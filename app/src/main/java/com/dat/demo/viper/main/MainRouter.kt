package com.dat.demo.viper.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit

class MainRouter(
    private val fragmentManager: FragmentManager,
    private val mainFragmentContainerId: Int
) : MainContracts.Router {

    private fun <T : Fragment> showFragment(fragmentClass: Class<T>) {
        fragmentManager.commit {
            replace(mainFragmentContainerId, fragmentClass, null)
        }
    }

    override fun showFirstTab() {
        showFragment(FragmentOne::class.java)
    }

    override fun showSecondTab() {
        showFragment(FragmentTwo::class.java)
    }
}