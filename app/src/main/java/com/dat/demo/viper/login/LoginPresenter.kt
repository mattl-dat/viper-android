package com.dat.demo.viper.login

import android.app.Activity
import android.content.Context
import com.dat.demo.viper.entities.AppState
import com.dat.demo.viper.entities.LoginManager

class LoginPresenter(
    applicationContext: Context,
    private val view: LoginContracts.View,
    private val router: LoginContracts.Router,
    appState: AppState = AppState(applicationContext)
) : LoginContracts.Presenter, LoginContracts.Interactor.Output {

    private val interactor: LoginContracts.Interactor = LoginInteractor(
        appState = appState,
        loginManager = LoginManager(appState = appState),
        output = this
    )

    override fun onLogin(username: String, password: String) {
        view.progressIndicatorVisible = true
        interactor.login(username, password)
    }

    override fun onLoginFailure(message: String) {
        view.progressIndicatorVisible = false
        view.showError(message)
    }

    override fun onLoginSuccess() {
        view.progressIndicatorVisible = false
        router.showHome()
    }

    override fun onStart() {
        view.setUsername(interactor.initialUsername)
    }

    companion object {
        fun <T> newInstance(activity: T): LoginPresenter where T : Activity, T : LoginContracts.View =
            LoginPresenter(
                applicationContext = activity.applicationContext,
                view = activity,
                router = LoginRouter(activity)
            )
    }
}