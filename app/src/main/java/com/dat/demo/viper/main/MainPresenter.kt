package com.dat.demo.viper.main

import androidx.fragment.app.FragmentActivity
import com.dat.demo.viper.R

class MainPresenter(
    private val view: MainContracts.View,
    private val router: MainContracts.Router
) : MainContracts.Presenter {

    override fun onSelectFirstTab() {
        router.showFirstTab()
    }

    override fun onSelectSecondtab() {
        router.showSecondTab()
    }

    companion object {
        fun <T> newInstance(activity: T) where T : FragmentActivity, T : MainContracts.View =
            MainPresenter(
                view = activity,
                router = MainRouter(
                    fragmentManager = activity.supportFragmentManager,
                    mainFragmentContainerId = R.id.main_container
                )
            )
    }
}